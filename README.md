# demo_gsk
## Requirements
Linux Platform, Python 3.6+ 

## Installation
1. Go to the Project Root Folder
2. Create virtualenv using 
```bash
vitrualenv venv -p python3
```
3. Activate virtualenv using 
```bash
source venv/bin/activate
```
4. Install requirements 
```bash
pip install -r requirements.txt
```
5. Create migrations using 
```bash
python manage.py makemigrations
```
6. Apply Migrations 
```bash
python manage.py migrate
```
7. Runserver on port 80 (make sure your port 80 is free)
```bash
python manage.py runserver 
```
If port 80 is not free you can use
```bash
python manage.py runserver localhost:<port>
```
