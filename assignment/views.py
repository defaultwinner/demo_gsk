import json
from django.shortcuts import render, get_object_or_404
from django.core.exceptions import SuspiciousOperation
from django.contrib.auth.models import User
from rest_framework import viewsets
from django.http import HttpResponse, HttpResponseBadRequest
from django.views import View
from django.http import Http404

from .models import (Employee, Project)

from .serializers import (EmployeeSerializer, ProjectSerializer)
from rest_framework.response import Response

# Create your views here.

class EmployeeViewSet(viewsets.ModelViewSet):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer

class ProjectViewSet(viewsets.ModelViewSet):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer