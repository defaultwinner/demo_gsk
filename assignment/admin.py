from django.contrib import admin

from .models import Employee, Project, ProjectEmployee
# Register your models here.

class ProjectAdmin(admin.ModelAdmin):
    fields = ('name', 'description', 'start_date')

admin.site.register(Employee)
admin.site.register(Project, ProjectAdmin)
admin.site.register(ProjectEmployee)