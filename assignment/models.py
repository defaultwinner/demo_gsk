from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import gettext_lazy as _
from django.utils import timezone

from .managers import CustomUserManager


class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_('email address'), unique=True)
    name = models.CharField(max_length=50)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.name
    
class Employee(CustomUser):
    employee_id = models.CharField(unique=True, max_length=6)

    def __str__(self):
        return self.employee_id

class Project(models.Model):
    project_id = models.CharField(unique=True, max_length=3)
    name = models.CharField(max_length=80)
    description = models.TextField(max_length=300)
    start_date = models.DateField()
    employees = models.ManyToManyField(Employee, through='ProjectEmployee')

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self.project_id = format(self.pk, '03d')
        super().save(*args, **kwargs)

class ProjectEmployee(models.Model):
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    date_joined = models.DateField()
    date_left = models.DateField(null=True, blank=True)
