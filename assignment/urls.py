from django.conf.urls import url, include
from django.urls import path
from rest_framework import routers, serializers, viewsets

from . import views
app_name = 'assignment'

gskrouter = routers.DefaultRouter()
gskrouter.register(r'employees', views.EmployeeViewSet, 'EmployeeDetails')
gskrouter.register(r'project', views.ProjectViewSet, 'ProjectDetails')

urlpatterns = [
    url(r'', include((gskrouter.urls, 'assignment'), namespace='assignment')),
]
