from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from .models import Employee, Project

class EmployeeSerializer(serializers.HyperlinkedModelSerializer):
    employee_id = serializers.CharField(
            min_length=6, max_length=6, allow_blank=False, trim_whitespace=True,
            validators=[UniqueValidator(queryset=Employee.objects.all())]
    )
    class Meta:
        model = Employee
        fields = ['name', 'employee_id']


class ProjectSerializer(serializers.HyperlinkedModelSerializer):
    employees = EmployeeSerializer(many=True)
    project_id = serializers.ReadOnlyField()
    class Meta:
        model = Project
        fields = ['project_id', 'name', 'description', 'start_date', 'employees']

    # def create(self, validated_data):
